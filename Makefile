#!/usr/bin/make -f

SQL_SRC=db.sql
SQL_DEST=/tmp/src/initdb.d/db.sql

fetch-sql:
	cp -v "${SQL_SRC}" "${SQL_DEST}"
